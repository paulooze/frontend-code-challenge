# React Pokedex app

This is a simple app to display pokemon in a grid/list view. I used Vite, @carbon/react, urql and graphql-codegen.

To run the app, just type:

```
npm run dev
```

To generate types against running GraphQL server, run:

```
npm run codegen
```

This whole thing is obviously not production ready, but the general idea is good.

I also fixed a small bug in the backend code (Pokemon.evolutions actually wasn't a list of pokemons - see git diff).
