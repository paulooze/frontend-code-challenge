import { Button, ButtonSet } from "@carbon/react";
import { FC } from "react";
import { PlaySoundButton } from "../../components/play-sound-button/play-sound-button";
import { Favorite, FavoriteFilled } from "@carbon/react/icons";
import { useFavoritesContext } from "../../providers/favorites-provider/use-favorites-context";

type Props = {
  pokemonId: string;
  soundURL: string;
  isFavorite: boolean;
};

export const PokemonActionButtons: FC<Props> = ({
  soundURL,
  isFavorite,
  pokemonId,
}) => {
  const {
    addToFavorites,
    removeFromFavorites,
    isFavoriteRemoveLoading,
    isFavoriteAddLoading,
  } = useFavoritesContext();
  return (
    <ButtonSet>
      <PlaySoundButton soundURL={soundURL} />
      <Button
        size="md"
        kind="primary"
        renderIcon={isFavorite ? FavoriteFilled : Favorite}
        onClick={() => {
          if (isFavorite) {
            return removeFromFavorites(pokemonId);
          }
          return addToFavorites(pokemonId);
        }}
        disabled={isFavoriteRemoveLoading || isFavoriteAddLoading}
      >
        {isFavorite ? "Unfavorite" : "Favorite"}
      </Button>
    </ButtonSet>
  );
};
