import { FC } from "react";
import { useParams } from "react-router-dom";
import { PageLayout } from "../../components/page-layout/page-layout";
import { gql, useQuery } from "@apollo/client";
import { Column, ProgressBar, Row, Tag, Tile } from "@carbon/react";
import {
  PokemonDetailQuery,
  PokemonDetailQueryVariables,
} from "../../__generated__/graphql";
import { unwrap } from "../../helpers/unwrap";
import { PokemonGridView } from "../../components/pokemon-grid-view/pokemon-grid-view";
import styles from "./pokemon-detail.module.scss";
import { PokemonActionButtons } from "./pokemon-action-buttons";

const POKEMON_DETAIL_QUERY = gql`
  query PokemonDetail($name: String!) {
    pokemonByName(name: $name) {
      id
      number
      name
      isFavorite
      image
      classification
      types
      sound
      maxHP
      maxCP
      weight {
        maximum
        minimum
      }
      height {
        maximum
        minimum
      }
      evolutions {
        id
        number
        name
        classification
        types
        image
        isFavorite
      }
    }
  }
`;

export const PokemonDetail: FC = () => {
  const { name } = useParams<{ name: string }>();
  const { data, loading, error } = useQuery<
    PokemonDetailQuery,
    PokemonDetailQueryVariables
  >(POKEMON_DETAIL_QUERY, {
    variables: { name: unwrap(name) },
  });

  if (name == null || error != null || data?.pokemonByName == null) {
    return <PageLayout pageTitle="Ooops...">Something went wrong</PageLayout>;
  }

  if (loading) {
    return <PageLayout pageTitle="..." />;
  }

  const { pokemonByName: pokemon } = data;

  return (
    <PageLayout
      pageTitle={
        <>
          <h1>{pokemon.name}</h1>
          <h2>{pokemon.classification}</h2>
        </>
      }
    >
      <Tile className={styles.mainTile}>
        <Row>
          <Column lg={8}>
            <img
              src={pokemon.image}
              alt={pokemon.name}
              className={styles.image}
            />
          </Column>
          <Column lg={8}>
            <div className={styles.stats}>
              <div className={styles.tagsWrap}>
                {pokemon.types.map((type) => (
                  <Tag key={type} title={type}>
                    {type}
                  </Tag>
                ))}
              </div>
              <ProgressBar
                max={pokemon.maxHP}
                value={pokemon.maxHP}
                label="Max HP"
                helperText={pokemon.maxHP.toString()}
                status="finished"
                className={styles.progressBar}
              />
              <ProgressBar
                max={pokemon.maxCP}
                value={pokemon.maxCP}
                label="Max CP"
                helperText={pokemon.maxCP.toString()}
                status="active"
                className={styles.progressBar}
              />
              <div className={styles.boxes}>
                <div className={styles.box}>
                  <h4>Weight</h4>
                  <span>
                    {pokemon.weight.minimum} - {pokemon.weight.maximum}
                  </span>
                </div>
                <div className={styles.box}>
                  <h4>Height</h4>
                  <span>
                    {pokemon.height.minimum} - {pokemon.height.maximum}
                  </span>
                </div>
              </div>
              <div className={styles.buttonSet}>
                <PokemonActionButtons
                  soundURL={pokemon.sound}
                  isFavorite={pokemon.isFavorite}
                  pokemonId={pokemon.id}
                />
              </div>
            </div>
          </Column>
        </Row>
      </Tile>
      {pokemon.evolutions.length > 0 && (
        <>
          <h2 className={styles.secondaryHeading}>Evolutions</h2>
          <PokemonGridView pokemons={pokemon.evolutions ?? []} />
        </>
      )}
    </PageLayout>
  );
};
