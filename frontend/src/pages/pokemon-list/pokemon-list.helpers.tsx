import { PokemonListQueryVariables } from "../../__generated__/graphql";

export type PaginationArgs = {
  limit: number;
  offset: number;
};
export const PAGE_SIZE = 12;
export const INITIAL_PAGINATION_ARGS: PaginationArgs = {
  limit: PAGE_SIZE,
  offset: 0,
};
export const INITIAL_FILTER_ARGS: Pick<
  PokemonListQueryVariables["query"],
  "filter" | "search"
> = {
  search: "",
  filter: {
    type: "",
    isFavorite: null,
  },
};

export type View = "grid" | "list";

export function ensureStringIsView(value: string): View {
  if (value !== "grid" && value !== "list") {
    throw new Error("should not happen #1");
  }
  return value;
}
