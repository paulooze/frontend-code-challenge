import { gql, useQuery } from "@apollo/client";
import {
  Column,
  FlexGrid,
  Row,
  Pagination,
  Tabs,
  TabList,
  Tab,
  Select,
  SelectItem,
} from "@carbon/react";
import { FC, useState } from "react";
import {
  PokemonListQuery,
  PokemonListQueryVariables,
} from "../../__generated__/graphql";
import { PokemonFilter } from "../../components/pokemon-filter/pokemon-filter";
import { PokemonGridView } from "../../components/pokemon-grid-view/pokemon-grid-view";
import {
  INITIAL_FILTER_ARGS,
  INITIAL_PAGINATION_ARGS,
  PaginationArgs,
  View,
  ensureStringIsView,
} from "./pokemon-list.helpers";
import styles from "./pokemon-list.module.scss";
import { PokemonListView } from "../../components/pokemon-list-view/pokemon-list-view";
import { PageLayout } from "../../components/page-layout/page-layout";

const POKEMON_LIST_QUERY = gql`
  query PokemonList($query: PokemonsQueryInput!) {
    pokemons(query: $query) {
      edges {
        id
        name
        types
        number
        isFavorite
        image
        classification
      }
      offset
      count
      limit
    }
  }
`;

export const PokemonList: FC = () => {
  const [selectedView, setSelectedView] = useState<View>("grid");
  const [selectedTabIndex, setSelectedTabIndex] = useState<number>(0);
  const [pagination, setPagination] = useState<PaginationArgs>(
    INITIAL_PAGINATION_ARGS,
  );
  const [filter, setFilter] = useState(INITIAL_FILTER_ARGS);
  const {
    data,
    loading: isPokemonQueryLoading,
    error,
  } = useQuery<PokemonListQuery, PokemonListQueryVariables>(
    POKEMON_LIST_QUERY,
    {
      context: {
        selectedTabIndex,
      },
      variables: {
        query: {
          ...pagination,
          ...filter,
        },
      },
    },
  );

  if (isPokemonQueryLoading) {
    return null;
  }

  if (error != null || data == null) {
    return null;
  }

  const { edges: pokemons, ...paginationResponse } = data.pokemons;

  const handleTabChange = ({ selectedIndex }: { selectedIndex: number }) => {
    setPagination(INITIAL_PAGINATION_ARGS);
    setFilter({
      ...INITIAL_FILTER_ARGS,
      filter: {
        ...INITIAL_FILTER_ARGS.filter,
        isFavorite: !!selectedIndex,
      },
    });
    setSelectedTabIndex(selectedIndex);
  };

  const handleTypeSelectChange = (type: string) => {
    setFilter((prev) => ({
      ...prev,
      filter: {
        ...prev.filter,
        type,
      },
    }));
  };

  const handleSearchChange = (value: string) => {
    setFilter((prev) => ({
      ...prev,
      search: value,
    }));
  };

  const handlePageChange = ({ page }: { page: number }) => {
    setPagination((prev) => ({
      ...prev,
      offset: (page - 1) * paginationResponse.limit,
    }));
  };

  return (
    <PageLayout pageTitle={<h1>Pokedex</h1>}>
      <div className={styles.filterRow}>
        <PokemonFilter
          selectedType={filter.filter?.type ?? ""}
          onTypeSelectChange={handleTypeSelectChange}
          initialSearchValue={filter.search ?? ""}
          onSearchChange={handleSearchChange}
        />
        <div className={styles.viewSelect}>
          <Select
            id="view-select"
            placeholder="Select view"
            labelText="Select view"
            value={selectedView}
            onChange={(event) => {
              setSelectedView(ensureStringIsView(event.target.value));
            }}
          >
            <SelectItem value="grid" text="Grid view" />
            <SelectItem value="list" text="List view" />
          </Select>
        </div>
      </div>
      <Tabs selectedIndex={selectedTabIndex} onChange={handleTabChange}>
        <TabList className={styles.tabs} aria-label="List of views">
          <Tab>All Pokemon</Tab>
          <Tab>Favorite Pokemon</Tab>
        </TabList>
      </Tabs>
      {selectedView === "grid" && <PokemonGridView pokemons={pokemons} />}
      {selectedView === "list" && <PokemonListView pokemons={pokemons} />}
      <Row>
        <Column>
          <Pagination
            page={
              (paginationResponse.offset + pagination.limit) /
              paginationResponse.limit
            }
            pageSize={pagination.limit}
            pageSizes={[pagination.limit]}
            totalItems={paginationResponse.count}
            onChange={handlePageChange}
          />
        </Column>
      </Row>
    </PageLayout>
  );
};
