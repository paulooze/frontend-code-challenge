import { useContext } from "react";
import { FavoritesContext, FavoritesContextProps } from "./favorites-provider";

export function useFavoritesContext(): FavoritesContextProps {
  const context = useContext(FavoritesContext);

  if (context == null) {
    throw new Error("FavoritesContext is missing");
  }
  return context;
}
