import { gql, useMutation } from "@apollo/client";
import { FC, PropsWithChildren, createContext, useMemo, useState } from "react";
import {
  FavoritePokemonMutation,
  FavoritePokemonMutationVariables,
  UnFavoritePokemonMutation,
  UnFavoritePokemonMutationVariables,
} from "../../__generated__/graphql";
import { ToastNotification } from "@carbon/react";
import styles from "./favorites-provider.module.scss";

export type FavoritesContextProps = {
  addToFavorites: (id: string) => void;
  removeFromFavorites: (id: string) => void;
  isFavoriteAddLoading: boolean;
  isFavoriteRemoveLoading: boolean;
};

export const FavoritesContext = createContext<FavoritesContextProps | null>(
  null,
);

const FAVORITE_POKEMON_MUTATION = gql`
  mutation FavoritePokemon($id: ID!) {
    favoritePokemon(id: $id) {
      id
      isFavorite
    }
  }
`;
const UNFAVORITE_POKEMON_MUTATION = gql`
  mutation UnFavoritePokemon($id: ID!) {
    unFavoritePokemon(id: $id) {
      id
      isFavorite
    }
  }
`;

type NotificationProps = {
  title: string;
  kind: "error" | "success";
};

export const FavoritesProvider: FC<PropsWithChildren> = ({ children }) => {
  const [toast, setToast] = useState<NotificationProps | null>(null);
  const [markAsFavorite, { loading: isFavoriteMutationLoading, client }] =
    useMutation<FavoritePokemonMutation, FavoritePokemonMutationVariables>(
      FAVORITE_POKEMON_MUTATION,
      { refetchQueries: ["PokemonList", "PokemonDetail"] },
    );
  const [unmarkAsFavorite, { loading: isUnFavoriteMutationLoading }] =
    useMutation<UnFavoritePokemonMutation, UnFavoritePokemonMutationVariables>(
      UNFAVORITE_POKEMON_MUTATION,
      { refetchQueries: ["PokemonList", "PokemonDetail"] },
    );

  const handlers = useMemo(
    () => ({
      addToFavorites: (id: string) => {
        client.clearStore();
        setToast(null);
        markAsFavorite({
          variables: { id },
        }).then((res) => {
          if (res.errors?.length) {
            return setToast({
              title: "Error adding to favorites",
              kind: "error",
            });
          }
          return setToast({
            title: "Success!",
            kind: "success",
          });
        });
      },
      removeFromFavorites: (id: string) => {
        client.clearStore();
        setToast(null);
        unmarkAsFavorite({
          variables: { id },
        }).then((res) => {
          if (res.errors?.length) {
            return setToast({
              title: "Error adding to favorites",
              kind: "error",
            });
          }
          return setToast({
            title: "Success!",
            kind: "success",
          });
        });
      },
      isFavoriteAddLoading: isFavoriteMutationLoading,
      isFavoriteRemoveLoading: isUnFavoriteMutationLoading,
    }),
    [
      client,
      isFavoriteMutationLoading,
      isUnFavoriteMutationLoading,
      markAsFavorite,
      unmarkAsFavorite,
    ],
  );

  return (
    <FavoritesContext.Provider value={handlers}>
      <div className={styles.toastNotifications}>
        {toast && (
          <ToastNotification
            className={styles.toastNotification}
            title={toast.title}
            kind={toast.kind}
            onCloseButtonClick={() => {
              setToast(null);
            }}
            timeout={2000}
          />
        )}
      </div>
      {children}
    </FavoritesContext.Provider>
  );
};
