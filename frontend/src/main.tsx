import React from "react";
import ReactDOM from "react-dom/client";
import App from "./app.tsx";
import "./index.scss";
import { ApolloClient, ApolloProvider, InMemoryCache } from "@apollo/client";
import { FavoritesProvider } from "./providers/favorites-provider/favorites-provider.tsx";

const client = new ApolloClient({
  uri: "http://localhost:4000/graphql",
  cache: new InMemoryCache(),
});

ReactDOM.createRoot(document.getElementById("root")!).render(
  <React.StrictMode>
    <ApolloProvider client={client}>
      <FavoritesProvider>
        <App />
      </FavoritesProvider>
    </ApolloProvider>
  </React.StrictMode>,
);
