import { BrowserRouter, Route, Routes } from "react-router-dom";
import { PokemonList } from "./pages/pokemon-list/pokemon-list";
import { PokemonDetail } from "./pages/pokemon-detail/pokemon-detail";

function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<PokemonList />} />
        <Route path="/:name" element={<PokemonDetail />} />
      </Routes>
    </BrowserRouter>
  );
}

export default App;
