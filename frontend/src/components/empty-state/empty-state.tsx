import { Column } from "@carbon/react";
import { FC } from "react";
import styles from "./empty-state.module.scss";

export const EmptyState: FC = () => {
  return (
    <Column lg={16} className={styles.emptyState}>
      <h1>No Pokemon found</h1>
    </Column>
  );
};
