import { Column, Row } from "@carbon/react";
import { FC } from "react";
import { PokemonListCard } from "../pokemon-list-card/pokemon-list-card";
import { EmptyState } from "../empty-state/empty-state";
import styles from "./pokemon-list-view.module.scss";

type Pokemon = {
  id: string;
  name: string;
  image: string;
  isFavorite: boolean;
  number: number;
  types: string[];
  classification: string;
};
type Props = {
  pokemons: Pokemon[];
};

export const PokemonListView: FC<Props> = ({ pokemons }) => {
  if (!pokemons.length) {
    return <EmptyState />;
  }

  return (
    <Row>
      {pokemons.map((pokemon) => (
        <Column lg={16} key={pokemon.id} className={styles.pokemonRow}>
          <PokemonListCard
            name={pokemon.name}
            imageURL={pokemon.image}
            number={pokemon.number}
            id={pokemon.id}
            isFavorite={pokemon.isFavorite}
            types={pokemon.types}
            classification={pokemon.classification}
          />
        </Column>
      ))}
    </Row>
  );
};
