import { Button } from "@carbon/react";
import { VolumeUpFilled } from "@carbon/react/icons";
import { FC, useRef } from "react";

type Props = {
  soundURL: string;
};

export const PlaySoundButton: FC<Props> = ({ soundURL }) => {
  const { current: audio } = useRef<HTMLAudioElement>(new Audio(soundURL));

  const handleButtonClick = () => {
    audio.play();
  };

  return (
    <Button
      size="md"
      renderIcon={VolumeUpFilled}
      onClick={handleButtonClick}
      label="Play pokemon sound"
      kind="secondary"
    >
      Play sound
    </Button>
  );
};
