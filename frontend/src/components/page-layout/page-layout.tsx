import { FC, PropsWithChildren, ReactNode } from "react";
import styles from "./page-layout.module.scss";

type Props = {
  pageTitle: ReactNode;
};

export const PageLayout: FC<PropsWithChildren<Props>> = ({
  children,
  pageTitle,
}) => {
  return (
    <div className={styles.pageWrap}>
      <header className={styles.pageHeader}>{pageTitle}</header>
      {children}
    </div>
  );
};
