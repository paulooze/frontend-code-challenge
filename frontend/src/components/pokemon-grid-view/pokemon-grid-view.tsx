import { Column, Row } from "@carbon/react";
import { FC } from "react";
import styles from "./pokemon-grid-view.module.scss";
import { PokemonGridCard } from "../pokemon-grid-card/pokemon-grid-card";
import { EmptyState } from "../empty-state/empty-state";

type Pokemon = {
  id: string;
  name: string;
  image: string;
  isFavorite: boolean;
  number: number;
  types: string[];
  classification: string;
};
type Props = {
  pokemons: Pokemon[];
};

export const PokemonGridView: FC<Props> = ({ pokemons }) => {
  if (!pokemons.length) {
    return <EmptyState />;
  }
  return (
    <Row>
      {pokemons.map((pokemon) => (
        <Column lg={8} key={pokemon.id} className={styles.pokemonColumn}>
          <PokemonGridCard
            name={pokemon.name}
            imageURL={pokemon.image}
            number={pokemon.number}
            id={pokemon.id}
            isFavorite={pokemon.isFavorite}
            types={pokemon.types}
            classification={pokemon.classification}
          />
        </Column>
      ))}
    </Row>
  );
};
