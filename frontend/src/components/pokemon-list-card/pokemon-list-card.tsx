import { Tag } from "@carbon/react";
import { FC } from "react";
import styles from "./pokemon-list-card.module.scss";
import { Link } from "react-router-dom";
import { FavoriteButton } from "../favorite-button/favorite-button";
import { useFavoritesContext } from "../../providers/favorites-provider/use-favorites-context";

type Props = {
  id: string;
  number: number;
  name: string;
  imageURL: string;
  isFavorite: boolean;
  types: string[];
  classification: string;
};

export const PokemonListCard: FC<Props> = ({
  id,
  name,
  imageURL,
  number,
  types,
  isFavorite,
  classification,
}) => {
  const {
    addToFavorites,
    isFavoriteAddLoading,
    removeFromFavorites,
    isFavoriteRemoveLoading,
  } = useFavoritesContext();
  return (
    <div className={styles.listItem}>
      <div className={styles.listImageWrap}>
        <Link to={`/${name}`}>
          <img
            className={styles.listImageWrapImage}
            src={imageURL}
            alt={name}
          />
        </Link>
      </div>
      <div className={styles.listItemContent}>
        <span>#{String(number).padStart(3, "0")}</span>{" "}
        <div className={styles.listItemHeader}>
          <h3>
            <Link to={`/${name}`}>{name}</Link>
          </h3>
          <FavoriteButton
            isFavorite={isFavorite}
            isDisabled={isFavoriteAddLoading || isFavoriteRemoveLoading}
            onClick={() => {
              if (isFavorite) {
                return removeFromFavorites(id);
              }
              return addToFavorites(id);
            }}
          />
        </div>
        <h4 className={styles.listItemSubheading}>{classification}</h4>
        <div>
          {types.map((type) => (
            <Tag key={type}>{type}</Tag>
          ))}
        </div>
      </div>
    </div>
  );
};
