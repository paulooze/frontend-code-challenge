import { Column, FlexGrid, Row } from "@carbon/react";
import { FC } from "react";
import { PokemonTypeSelect } from "./pokemon-type-select";
import { PokemonSearchInput } from "./pokemon-search-input";
import styles from "./pokemon-filter.module.scss";

type Props = {
  selectedType: string;
  onTypeSelectChange: (value: string) => void;
  initialSearchValue: string;
  onSearchChange: (value: string) => void;
};

export const PokemonFilter: FC<Props> = ({
  selectedType,
  onTypeSelectChange,
  initialSearchValue,
  onSearchChange,
}) => {
  return (
    <div className={styles.filterRow}>
      <div className={styles.selectWrap}>
        <PokemonTypeSelect value={selectedType} onChange={onTypeSelectChange} />
      </div>
      <div className={styles.inputWrap}>
        <PokemonSearchInput
          initialValue={initialSearchValue}
          onSubmit={onSearchChange}
        />
      </div>
    </div>
  );
};
