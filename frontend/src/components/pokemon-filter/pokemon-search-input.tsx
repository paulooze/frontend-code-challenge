import { Button, Column, FlexGrid, Row, Search } from "@carbon/react";
import { FC, useState } from "react";
import styles from "./pokemon-search-input.module.scss";

type Props = {
  onSubmit: (value: string) => void;
  initialValue: string;
};

export const PokemonSearchInput: FC<Props> = ({ onSubmit, initialValue }) => {
  const [value, setValue] = useState<string>(initialValue);
  return (
    <form
      className={styles.searchForm}
      onSubmit={(event) => {
        event.preventDefault();
        onSubmit(value);
      }}
    >
      <Search
        id="pokemon-search-input"
        labelText="Search Pokemon by name"
        placeholder="Search by name"
        onChange={(event) => {
          setValue(event.target.value);
        }}
        value={value}
      />
      <div className={styles.searchFormButton}>
        <Button size="md" type="submit">
          Search
        </Button>
      </div>
    </form>
  );
};
