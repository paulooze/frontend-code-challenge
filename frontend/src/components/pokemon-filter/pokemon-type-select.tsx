import { useQuery } from "@apollo/client";
import { Select, SelectItem, SelectSkeleton } from "@carbon/react";
import gql from "graphql-tag";
import { FC } from "react";
import { PokemonTypesQuery } from "../../__generated__/graphql";

const POKEMON_TYPES_QUERY = gql`
  query PokemonTypes {
    pokemonTypes
  }
`;

type Props = {
  value: string;
  onChange: (value: string) => void;
};

export const PokemonTypeSelect: FC<Props> = ({ onChange, value }) => {
  const { data, loading, error } =
    useQuery<PokemonTypesQuery>(POKEMON_TYPES_QUERY);

  if (loading) {
    return <SelectSkeleton />;
  }

  if (error != null || data == null) {
    return null;
  }

  return (
    <Select
      id="pokemon-type"
      placeholder="Select pokemon type"
      labelText="Filter Pokemon by type"
      onChange={(event) => {
        onChange(event.target.value);
      }}
      value={value}
    >
      <SelectItem value="" text="All types" />
      {data.pokemonTypes.map((type) => (
        <SelectItem value={type} text={type} key={type} />
      ))}
    </Select>
  );
};
