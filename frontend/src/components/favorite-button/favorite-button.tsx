import { IconButton } from "@carbon/react";
import { Favorite, FavoriteFilled } from "@carbon/react/icons";
import { FC } from "react";
import styles from "./favorite-button.module.scss";

type Props = {
  onClick: () => void;
  isFavorite: boolean;
  isDisabled: boolean;
};

export const FavoriteButton: FC<Props> = ({
  onClick,
  isFavorite,
  isDisabled,
}) => {
  return (
    <IconButton
      kind="ghost"
      disabled={isDisabled}
      className={styles.favoritesButton}
      size="lg"
      label={isFavorite ? "Remove from favorites" : "Add to favorites"}
      onClick={onClick}
    >
      {isFavorite ? <FavoriteFilled /> : <Favorite />}
    </IconButton>
  );
};
